extends KinematicBody2D

var mini_laser = preload("res://scene/MiniLaser.tscn")

var health = 10 setget set_health
var shield = 5
var laser_charge = 100
var current_velocity = Vector2()

var time_not_dead = 0
var elapse = 0

signal dead

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("player")
	laser_charge = 100
	time_not_dead = OS.get_ticks_msec()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	look_at(get_global_mouse_position())
	
	var vel = Vector2()
	vel.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	vel.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	
	var motion = lerp(current_velocity ,vel.normalized() * 300, 0.1)
	current_velocity = motion
	move_and_slide(motion)
	
	if Input.is_action_just_pressed("ui_accept"):
		
		var shot_vel = Vector2(1000, 0)
		var shot_pos = Vector2(5, 25)
		var shot2_pos = Vector2(5, -25)
		shot_vel = shot_vel.rotated(rotation)
		shot_pos = shot_pos.rotated(rotation)
		shot2_pos = shot2_pos.rotated(rotation)
		
		var music = AudioStreamPlayer.new()
		self.add_child(music)
		music.stream = load('res://snd/sfx_laser1.ogg')
		music.play()
		
		$Camera2D.shake_amount = 0.1
		var shot1 = mini_laser.instance()
		shot1.position = global_position
		shot1.position = shot1.position + shot_pos
		shot1.rotation_degrees = rotation_degrees
		shot1.velocity = shot_vel
		$MiniLaserScene.add_child(shot1)
		var shot2 = mini_laser.instance()
		shot2.position = global_position
		shot2.position = shot2.position + shot2_pos
		shot2.rotation_degrees = rotation_degrees
		shot2.velocity = shot_vel
		$MiniLaserScene.add_child(shot2)
		
	if Input.is_action_pressed("ui_select"):
		if $Laser.is_casting:
			if laser_charge <= 0:
				$Laser.is_casting = false
			pass # Do nothing
		else:
			if laser_charge == 100:
				$Laser.is_casting = true
			else:
				pass
	else:
		if $Laser.is_casting:
			$Laser.is_casting = false
	
	$Gui/Health.value = health
	$Gui/Shield.value = shield
	$Gui/LaserCharge.value = laser_charge
	
	var time_now = OS.get_ticks_msec()
	elapse = int((time_now - time_not_dead) / 1000.0)
	$Gui/TimerLabel.text = "Survived " + str(elapse) + " secs"
	
	#Global.time = elapse
	pass

func set_health(value):
	if value > 10:
		value = 10
		
	if shield > 0:
		shield = shield - (health-value)
	else:
		health = value
		pass
		
	if health <= 0:
		#print("test2")
		emit_signal('dead', elapse)
		#get_tree().change_scene("res://scene/LoginScreen.tscn")
	pass
	


func _on_ShieldTimer_timeout():
	if shield < 5:
		shield = shield + 1
		
	pass # Replace with function body.


func _on_LaserCharge_timeout():
	if not $Laser.is_casting:
		laser_charge = laser_charge + 2
	else:
		laser_charge = laser_charge - 4
		
	if laser_charge > 100:
		laser_charge = 100
	
	pass # Replace with function body.

extends Area2D

var mini_laser = preload("res://scene/MiniLaser.tscn")

var health = 5 setget set_health
var velocity = Vector2(0,0)
var side = 1

var player_instance = null

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("enemy")
	side = randi() % 2
	if side == 0:
		side = -1
		
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	var overlapping_areas = $DetectionZone.get_overlapping_bodies()
	for bodies in overlapping_areas:
		if bodies.is_in_group("player"):
			player_instance = bodies
			
	if player_instance != null:
		rotation = rotation + get_angle_to(player_instance.position)
			
		randomize()
		
		velocity = Vector2(rand_range(50,100)*delta, side*rand_range(250,350)*delta)
		velocity = velocity.rotated(rotation)
	
	translate(velocity)
	pass

func set_health(value):
	if value > 5:
		health = 5
		
	health = value
	
	$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.05)
	$Tween.start()
	
	if health <= 0:
		queue_free()
	pass


func _on_ShotTimer_timeout():
	if player_instance != null:
		var shot_vel = Vector2(600, 0)
		var shot_pos = Vector2(5, 25)
		var shot2_pos = Vector2(5, -25)
		shot_vel = shot_vel.rotated(rotation)
		shot_pos = shot_pos.rotated(rotation)
		shot2_pos = shot2_pos.rotated(rotation)
		
		var shot1 = mini_laser.instance()
		shot1.position = global_position
		shot1.position = shot1.position + shot_pos
		shot1.rotation_degrees = rotation_degrees
		shot1.velocity = shot_vel
		shot1.modulate = Color(3, 0, 0)
		$MiniLaserScene.add_child(shot1)
		var shot2 = mini_laser.instance()
		shot2.position = global_position
		shot2.position = shot2.position + shot2_pos
		shot2.rotation_degrees = rotation_degrees
		shot2.velocity = shot_vel
		shot2.modulate = Color(3, 0, 0)
		$MiniLaserScene.add_child(shot2)
	pass # Replace with function body.

extends Node2D

var enemy = preload("res://scene/Enemy1.tscn")

var global_enemy_counter = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Player.connect("dead", self, "player_dead")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	
	var en = enemy.instance()
	randomize()
	en.position.x = rand_range(100, 2000-100)
	en.position.y = rand_range(100, 2000-100)
	add_child(en)
	
	global_enemy_counter += 1
	if global_enemy_counter > 5:
		global_enemy_counter = 0
		$Timer.wait_time = $Timer.wait_time * 0.7
	
	pass # Replace with function body.

func player_dead(value):
	$CanvasLayer/Pause/Label.text = "Congratulation! You survived " + str(value) + " secs."
	$CanvasLayer/Pause.set_pause()
	pass
